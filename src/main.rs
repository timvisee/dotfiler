#[macro_use]
extern crate derive_builder;
#[macro_use]
extern crate failure;

mod action;
mod cmd;
mod dot;
mod error;
mod profile;
mod util;

use std::process;

use clap::{crate_description, crate_name, crate_version};

use crate::action::{generate::Generate, install::Install, uninstall::Uninstall};
use crate::cmd::{
    matcher::{MainMatcher, Matcher},
    Handler,
};
use crate::error::Error;
use crate::util::{bin_name, highlight, quit_error, ErrorHints};

fn main() {
    // Parse CLI arguments
    let cmd_handler = Handler::parse();

    // Invoke the proper action
    if let Err(err) = invoke_action(&cmd_handler) {
        quit_error(err, ErrorHints::default());
    };
}

/// Invoke the proper action based on the CLI input.
///
/// If no proper action is selected, the program will quit with an error
/// message.
fn invoke_action(handler: &Handler) -> Result<(), Error> {
    // Match the generate command
    if handler.generate().is_some() {
        return Generate::new(handler.matches())
            .invoke()
            .map_err(|err| err.into());
    }

    // Match the install command
    if handler.install().is_some() {
        return Install::new(handler.matches())
            .invoke()
            .map_err(|err| err.into());
    }

    // Match the uninstall command
    if handler.uninstall().is_some() {
        return Uninstall::new(handler.matches())
            .invoke()
            .map_err(|err| err.into());
    }

    // Get the main matcher
    let matcher_main = MainMatcher::with(handler.matches()).unwrap();

    // Print the main info and return
    if !matcher_main.quiet() {
        print_main_info();
    }
    Ok(())
}

/// Print the main info, shown when no subcommands were supplied.
pub fn print_main_info() -> ! {
    // Get the name of the used executable
    let bin = bin_name();

    // Print the main info
    println!("{} {}", crate_name!(), crate_version!());
    println!("Usage: {} [FLAGS] <SUBCOMMAND> ...", bin);
    println!();
    println!(crate_description!());
    println!();
    println!("Missing subcommand. Here are the most used:");
    println!("    {}", highlight(&format!("{} install ...", bin)));
    println!("    {}", highlight(&format!("{} uninstall ...", bin)));
    println!();
    println!("To show all subcommands, features and other help:");
    println!("    {}", highlight(&format!("{} help [SUBCOMMAND]", bin)));

    process::exit(1)
}
