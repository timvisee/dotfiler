use clap::ArgMatches;
use failure::Fail;

use crate::cmd::matcher::{InstallMatcher, MainMatcher, Matcher};
use crate::dot::Dir;
use crate::profile::Profile;

/// A file install action.
pub struct Install<'a> {
    cmd_matches: &'a ArgMatches<'a>,
}

impl<'a> Install<'a> {
    /// Construct a new install action.
    pub fn new(cmd_matches: &'a ArgMatches<'a>) -> Self {
        Self { cmd_matches }
    }

    /// Invoke the install action.
    // TODO: create a trait for this method
    pub fn invoke(&self) -> Result<(), Error> {
        // Create the command matchers
        let matcher_main = MainMatcher::with(self.cmd_matches).unwrap();
        let _matcher_install = InstallMatcher::with(self.cmd_matches).unwrap();

        // Test: load sample dottr directory
        let dir = Dir::from_path(matcher_main.config()).expect("failed to load config");
        println!("dir: {:?}", dir);

        // Create profile for this installation
        let profile = Profile::from_with(&dir.config, |_| true);

        dbg!(profile);

        // TODO: load configuration
        println!("TODO: run installation process here");

        Ok(())
    }
}

#[derive(Debug, Fail)]
pub enum Error {
    /// An error occurred while installing dotfiles.
    #[fail(display = "")]
    Install,
}
