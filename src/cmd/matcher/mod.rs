pub mod generate;
pub mod install;
pub mod main;
pub mod uninstall;

// Re-export to matcher module
pub use self::generate::GenerateMatcher;
pub use self::install::InstallMatcher;
pub use self::main::MainMatcher;
pub use self::uninstall::UninstallMatcher;

use clap::ArgMatches;

pub trait Matcher<'a>: Sized {
    // Construct a new matcher instance from these argument matches.
    fn with(matches: &'a ArgMatches) -> Option<Self>;
}
