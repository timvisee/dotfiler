use clap::ArgMatches;

use super::Matcher;

/// The uninstall command matcher.
pub struct UninstallMatcher<'a> {
    matches: &'a ArgMatches<'a>,
}

impl<'a: 'b, 'b> UninstallMatcher<'a> {}

impl<'a> Matcher<'a> for UninstallMatcher<'a> {
    fn with(matches: &'a ArgMatches) -> Option<Self> {
        matches
            .subcommand_matches("uninstall")
            .map(|matches| Self { matches })
    }
}
