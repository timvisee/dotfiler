use clap::{App, SubCommand};

/// The uninstall command definition.
pub struct CmdUninstall;

impl CmdUninstall {
    pub fn build<'a, 'b>() -> App<'a, 'b> {
        // Build the subcommand
        SubCommand::with_name("uninstall")
            .about("Uninstall dotfiles")
            .visible_alias("u")
    }
}
