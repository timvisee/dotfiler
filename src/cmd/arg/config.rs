use std::path::{Path, PathBuf};

use clap::{Arg, ArgMatches};

use super::{CmdArg, CmdArgOption};

/// The config argument.
pub struct ArgConfig {}

impl CmdArg for ArgConfig {
    fn name() -> &'static str {
        "config"
    }

    fn build<'b, 'c>() -> Arg<'b, 'c> {
        Arg::with_name("config")
            .long("config")
            .short("c")
            .value_name("FILE")
            .default_value("./dot.yml")
            .global(true)
            .help("Custom dots configuration file")
    }
}

impl<'a> CmdArgOption<'a> for ArgConfig {
    type Value = PathBuf;

    fn value<'b: 'a>(matches: &'a ArgMatches<'b>) -> Self::Value {
        Path::new(Self::value_raw(matches).expect("missing config path")).into()
    }
}
