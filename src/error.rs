use crate::action::generate::completions::Error as CliGenerateCompletionsError;
use crate::action::install::Error as CliInstallError;
use crate::action::uninstall::Error as CliUninstallError;

#[derive(Fail, Debug)]
pub enum Error {
    /// An error occurred while invoking an action.
    #[fail(display = "")]
    Action(#[cause] ActionError),
}

impl From<CliInstallError> for Error {
    fn from(err: CliInstallError) -> Error {
        Error::Action(ActionError::Install(err))
    }
}

impl From<CliUninstallError> for Error {
    fn from(err: CliUninstallError) -> Error {
        Error::Action(ActionError::Uninstall(err))
    }
}

impl From<ActionError> for Error {
    fn from(err: ActionError) -> Error {
        Error::Action(err)
    }
}

#[derive(Debug, Fail)]
pub enum ActionError {
    /// An error occurred while generating completions.
    #[fail(display = "failed to generate shell completions")]
    GenerateCompletions(#[cause] CliGenerateCompletionsError),

    /// An error occurred while invoking the install action.
    #[fail(display = "failed to install dotfiles")]
    Install(#[cause] CliInstallError),

    /// An error occurred while invoking the uninstall action.
    #[fail(display = "failed to uninstall dotfiles")]
    Uninstall(#[cause] CliUninstallError),
}

impl From<CliGenerateCompletionsError> for ActionError {
    fn from(err: CliGenerateCompletionsError) -> ActionError {
        ActionError::GenerateCompletions(err)
    }
}
