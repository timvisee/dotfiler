use std::path::PathBuf;

use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Link {
    /// The dotfile path.
    from: PathBuf,

    /// The dotfile target path.
    to: PathBuf,
}
