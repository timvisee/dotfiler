use std::path::{Path, PathBuf};

use crate::dot::Config;

#[derive(Debug)]
pub struct Dir {
    /// Dot directory path.
    path: PathBuf,

    /// Dot config.
    pub config: Config,
}

impl Dir {
    // TODO: add error type
    pub fn from_path<P: AsRef<Path>>(path: P) -> Result<Self, ()> {
        Ok(Self {
            path: path.as_ref().into(),
            config: Config::load_path(path)?,
        })
    }
}
