[![Build status on GitLab CI][gitlab-ci-master-badge]][gitlab-ci-link]
[![Newest release on crates.io][crate-version-badge]][crate-link]
[![Project license][crate-license-badge]](LICENSE)

[crate-license-badge]: https://img.shields.io/crates/l/dottr.svg
[crate-link]: https://crates.io/crates/dottr
[crate-version-badge]: https://img.shields.io/crates/v/dottr.svg
[gitlab-ci-link]: https://gitlab.com/timvisee/dottr/pipelines
[gitlab-ci-master-badge]: https://gitlab.com/timvisee/dottr/badges/master/pipeline.svg

# dottr
A better way to manage your dotfiles.

Features (idea, planned or implemented):
- Clean YAML configuration in repository
- Declarative, hierarchical, modular and extensible configuration
- Profiles (multiple, customizable)
- Easily sync with single command
- Install & remove across versions with file tracking
- Automatically back-up existing configurations
- Configuration templating and merging (well-known types)
- Effortlessly use multiple (remote) dotfile sources

## License
This project is released under the GNU GPL-3.0 license.
Check out the [LICENSE](./LICENSE) file for more information.
